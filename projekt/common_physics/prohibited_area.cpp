/** @file prohibited_area.cpp
 *  @brief implementation of prohibited areas, used for simple collisions
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#include "stdafx.h"
#include "prohibited_area.h"

///////////////////////////////////////////////////////////////////////////////
// ProhibitedArea
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
// PlaneArea
///////////////////////////////////////////////////////////////////////////////
bool PlaneArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	double dx =pos.x - m_point.x;
	double dy =pos.y - m_point.y;
	double dz =pos.z - m_point.z;

	double nx =m_normal.x *dx;
	double ny =m_normal.y *dy;
	double nz =m_normal.z *dz;
	//double dist = m_normal.x * (pos.x - m_point.x) +  m_normal.y * (pos.y - m_point.y) +  m_normal.z * (pos.z - m_point.z);
	double dist = nx+ny+nz;
	double b =dist-margin;
	if (b < 0.0)
	{	
		if(pos.x >x2 ||pos.x <x1 || pos.z >z1 || pos.z <z3)
			return false;

		if (normal) {
			*normal = m_normal;
		}
		return true;
	}

	return false;
}



///////////////////////////////////////////////////////////////////////////////
// ComplesArea
///////////////////////////////////////////////////////////////////////////////
bool ComplexArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	for (size_t i = 0; i < m_areas.size(); ++i)
	{
		if (m_areas[i]->IsInArea(pos, margin, normal)) return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// MaterialPointSetWithProhibitedArea
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////


void MaterialPointSetWithProhibitedArea::Update(double deltaTime)
{
	BeforeStep(deltaTime);
	
	for (unsigned int i = 0; i < m_count; ++i)
	{
		if (m_bounds[i] == false)
		{
			
			m_points[i].PrepareMove(deltaTime, Force(i));

			if (m_prohibitedArea)
			{
				if (m_prohibitedArea->IsInArea(m_points[i].m_nextPos, m_points[i].m_radius, NULL))
				{
					UpdateOnCollision(i, deltaTime);
				}
			}
		}
	}

	AfterStep(deltaTime);

	for (unsigned int i = 0; i < m_count; ++i)
	{
		if (m_bounds[i] == false)
			m_points[i].UpdateMove();
	}
}

///////////////////////////////////////////////////////////////////////////////
void MaterialPointSetWithProhibitedArea::UpdateOnCollision(int id, double deltaTime)
{
	MaterialPoint *point = &m_points[id];
	Vec3d normal(0, 0, 0);
	if (m_prohibitedArea->IsInArea(point->m_nextPos, point->m_radius, &normal))
	{
		Vec3d force = Force(id);
		double normalFactor=DotProduct(force,normal);
		if(normalFactor<0){
			force-=normal * normalFactor;
		}

		double speedFactor = DotProduct(point->m_vel,normal);
		//if(speedFactor<0){
			point->m_vel-=(1+m_prohibitedArea->m_bounceFactor)*speedFactor* normal ;
		//}

		if(normalFactor<0 &&point->m_vel.Length()>0.0 ){
			Vec3d friction = -point->m_vel/10;
			friction.Normalize();
			friction *=fabs(m_prohibitedArea->m_frictionFactor*normalFactor);
			force+=friction;
		}
		point->PrepareMoveEuler(deltaTime, force);
	}
}