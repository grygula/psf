/** @file prohibited_area.h
 *  @brief declaration of prohibited areas, used for simple collisions
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#pragma once

#include <assert.h>
#include "utils_math.h"
#include "material_point.h"

class ProhibitedArea
{
public:
	double m_bounceFactor;
	double m_frictionFactor;
public:
	ProhibitedArea() { m_bounceFactor = 0.0; m_frictionFactor = 0.0; }
	ProhibitedArea(double bounceFactor, double frictionFactor) : m_bounceFactor(bounceFactor), m_frictionFactor(frictionFactor) { }
	virtual ~ProhibitedArea() { }

	virtual bool IsInArea(Vec3d &pos, double margin, Vec3d *normal) = 0;
};


class PlaneArea : public ProhibitedArea
{
public:
	Vec3d m_normal;
	Vec3d m_point;
	double x1,x2,x3,x4;
	double z1,z2,z3,z4;
	double y1,y2,y3,y4;
	

public:
	PlaneArea(const Vec3d &n, const Vec3d &p) : m_normal(n), m_point(p) { }
	PlaneArea(const Vec3d &n, const Vec3d &p, double bounceF, double frictionF) : ProhibitedArea(bounceF, frictionF), m_normal(n), m_point(p) { }
	virtual bool IsInArea(Vec3d &pos, double margin, Vec3d *normal);
	
	void setPoints(double sx1,double sx2,	double sx3,double sx4,double sz1,double sz2,double sz3,double sz4){
		x1=sx1;
		x2=sx2;
		x3=sx3;
		x4=sx4;
		z1=sz1;
		z2=sz2;
		z3=sz3;
		z4=sz4;
		updatePoints();
	}
	void setPoints(double smaxX,double sminX,double smaxZ,double sminZ){
		x1=x4=sminX;
		x2=x3=smaxX;
		z1=z2=smaxZ;
		z3=z4=sminZ;
		updatePoints();
	}
	void setPoints(double smax,double smin){
		x1=x4=z3=z4=smin;
		x2=x3=z1=z2=smax;
		updatePoints();
	}
	void updatePoints(){
		double d = -(m_normal.x * m_point.x +m_normal.y * m_point.y+m_normal.z * m_point.z);

		y1=-((d+m_normal.x * x1+m_normal.z * z1))/m_normal.y;
		y2=-((d+m_normal.x * x2+m_normal.z * z2))/m_normal.y;
		y3=-((d+m_normal.x * x3+m_normal.z * z3))/m_normal.y;
		y4=-((d+m_normal.x * x4+m_normal.z * z4))/m_normal.y;

		double s=0.0;
	}
};


class ComplexArea : public ProhibitedArea
{
protected:
	std::vector<ProhibitedArea *> m_areas;
public:
	ComplexArea(double bounceF, double frictionF) : ProhibitedArea(bounceF, frictionF) { }
	virtual ~ComplexArea() { }

	void Add(ProhibitedArea *area, bool setFactors = true) { if (setFactors) { area->m_bounceFactor = m_bounceFactor; } area->m_frictionFactor = m_frictionFactor; m_areas.push_back(area); }
	int SubAreasCount() const { return (int)m_areas.size(); }

	virtual bool IsInArea(Vec3d &pos, double margin, Vec3d *normal);
};

///////////////////////////////////////////////////////////////////////////////
class MaterialPointSetWithProhibitedArea : public MaterialPointSet
{
public:
	ProhibitedArea *m_prohibitedArea;

public:
	MaterialPointSetWithProhibitedArea(int count) : MaterialPointSet(count), m_prohibitedArea(NULL) { }
	MaterialPointSetWithProhibitedArea(int count, ProhibitedArea *area) : MaterialPointSet(count), m_prohibitedArea(area) { }
	virtual ~MaterialPointSetWithProhibitedArea() { }

	virtual void Update(double deltaTime);
protected:
	void UpdateOnCollision(int id, double deltaTime);
};