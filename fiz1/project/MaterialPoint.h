#pragma once
#include "TVector3.h"

enum calcType{
	atEuler,
	atVolr
};
class MaterialPoint
{

public:
	Vec3d m_nextPos;
	Vec3d m_pos;
	Vec3d m_prevPos;
	Vec3d m_nextVel;
	Vec3d m_vel;
	Vec3d m_acceleration;
	double m_mass;
	double m_radius;
	unsigned int m_stepCounter;
protected:
	calcType static const m_algType = atEuler;
public:
	MaterialPoint(void);
	~MaterialPoint(void);
	void PrepareMove(double deltaTime, const Vec3d &force);
	void UpdateMove();
	void Reset();
protected:
	void CalcEuler(double delta);
	void CalcVerlet(double delta);
};

