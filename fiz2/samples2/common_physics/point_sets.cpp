/** @file point_sets.cpp
 *  @brief implementation of different point sets
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#include "stdafx.h"
#include "material_point.h"
#include "point_sets.h"

///////////////////////////////////////////////////////////////////////////////
// OscilatorPointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
OscilatorPointSet::OscilatorPointSet(unsigned int count) : MaterialPointSet(count)
{
	m_baseLen = 0.0;
	m_k = 0.0;
	m_resistance = 0.0;
	m_suppression = 0.0;
	m_forceOnStretch = true;
}

///////////////////////////////////////////////////////////////////////////////
Vec3d OscilatorPointSet::Force(unsigned int i)
{
	Vec3d forceLeft= Vec3d(0,0,0), forceRight=Vec3d(0,0,0);
	double zero = 0.0f;
	if(i>0){
		Vec3d left = Point(i-1)->m_pos - Point(i)->m_pos;
		
		//test
		Vec3d left1 = Point(i-1)->m_pos - Point(i)->m_pos;
		Vec3d left2 = Point(i)->m_pos - Point(i-1)->m_pos;
		double l1=left1.Length();
		double l2 = left2.Length();
		double distance =left.Length()-m_baseLen;
		if(m_forceOnStretch||distance>zero){
			left.Normalize();
			forceLeft=m_k*distance*left;
			Vec3d v = m_points[i-1].m_vel-m_points[i].m_vel;
			forceLeft-=-2*m_suppression*DotProduct(left,v)*left;
		}
	}
	if(i<(PointCount()-1)){
		Vec3d right = m_points[i+1].m_pos - m_points[i].m_pos;
		double distance = right.Length()-m_baseLen;
		if(m_forceOnStretch||distance>zero){
			right.Normalize();
			forceRight=m_k*distance*right;
			Vec3d v = m_points[i+1].m_vel-m_points[i].m_vel;
			forceRight-=-2*m_suppression*DotProduct(right,v)*right;
		}
	}
	Vec3d force = forceLeft+forceRight;
	Vec3d fResistance =-2*m_resistance* Point(i)->m_vel;
	force+=fResistance;
	
	return force;
}

///////////////////////////////////////////////////////////////////////////////
// StiffOscilatorPointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
StiffOscilatorPointSet::StiffOscilatorPointSet(unsigned int count) :
	OscilatorPointSet(count)
{
	m_stiffnessForces = new Vec3d[count];
	m_forceOnStretch = true;
}

///////////////////////////////////////////////////////////////////////////////
Vec3d StiffOscilatorPointSet::Force(unsigned int i)
{
	Vec3d force = OscilatorPointSet::Force(i);
	if (m_stiffness > 0.0) {
		force += m_stiffnessForces[i];
	}
	return force;
}

///////////////////////////////////////////////////////////////////////////////
void StiffOscilatorPointSet::BeforeStep(double deltaTime)
{
	OscilatorPointSet::BeforeStep(deltaTime);
	for (unsigned int i = 0; i < m_count; ++i)
	{
		m_stiffnessForces[i]=0;
	}
	for (unsigned int i = 1; i < m_count-1; ++i)
	{
		Vec3d leftDistance = m_points[i-1].m_pos - m_points[i].m_pos;
		Vec3d rightDisance = m_points[i+1].m_pos - m_points[i].m_pos;
		Vec3d stiffnessForce =m_stiffness * (rightDisance+leftDistance)/2 ;
			m_stiffnessForces[i]+=stiffnessForce;
			m_stiffnessForces[i-1]-=stiffnessForce/2;
			m_stiffnessForces[i+1]-=stiffnessForce/2;
	}
	

	/* pseudokod: ************************
	function Sila(i)
	{
		return sila_wlasciwa(i) + sila_sztywnosci[i]
	}

	*************************************/ 
	
	///////////////////
	// TODO
	///////////////////
}

///////////////////////////////////////////////////////////////////////////////
// LinePointSet
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
LinePointSet::LinePointSet(unsigned int count) :
	StiffOscilatorPointSet(count),
	m_gForce(0.0, -9.0, 0.0)
{
	m_forceOnStretch = true;
}


