/** @file prohibited_area.cpp
 *  @brief implementation of prohibited areas, used for simple collisions
 *
 *	@author Bartlomiej Filipek
 *	@date April 2011
 */

#include "stdafx.h"
#include "prohibited_area.h"

///////////////////////////////////////////////////////////////////////////////
// ProhibitedArea
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
// FloorArea
///////////////////////////////////////////////////////////////////////////////
bool FloorArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	bool result = (pos.y<m_level+margin);
	if(result && normal!=NULL) *normal=Vec3d(0,1,0);
	return result;
}

///////////////////////////////////////////////////////////////////////////////
// PlaneArea
///////////////////////////////////////////////////////////////////////////////
bool PlaneArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	///////////////////
	// TODO
	///////////////////

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// SphereArea
///////////////////////////////////////////////////////////////////////////////
bool SphereArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	Vec3d radiusVec=pos-m_center;
	bool result = radiusVec.Length()<m_radius+margin;
	if(result && normal!=NULL) {
		*normal=radiusVec;
		normal->Normalize();
	}
	return result;

}

///////////////////////////////////////////////////////////////////////////////
// InsideBoxArea
///////////////////////////////////////////////////////////////////////////////
bool InsideBoxArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	double mnx = m_min.x+margin,max = m_max.x-margin;
	double mny = m_min.y+margin,may = m_max.y-margin;
	double mnz = m_min.z+margin,maz = m_max.z-margin;
	bool result = pos.x<mnx || pos.x>max ||pos.y<mny || pos.y>may||pos.z<mnz || pos.z>maz;
	if(result && normal!=NULL){
		if(pos.x<mnx) normal->x=1;
		if(pos.x>max) normal->x=-1;

		if(pos.y<mny) normal->y=1;
		if(pos.y>may) normal->y=-1;

		if(pos.z<mnz) normal->z=1;
		if(pos.z>maz) normal->z=-1;
	}
	return result;
}

///////////////////////////////////////////////////////////////////////////////
// ComplesArea
///////////////////////////////////////////////////////////////////////////////
bool ComplexArea::IsInArea(Vec3d &pos, double margin, Vec3d *normal)
{
	for (size_t i = 0; i < m_areas.size(); ++i)
	{
		if (m_areas[i]->IsInArea(pos, margin, normal)) return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////
// MaterialPointSetWithProhibitedArea
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
void MaterialPointSetWithProhibitedArea::Update(double deltaTime)
{
	BeforeStep(deltaTime);
	
	for (unsigned int i = 0; i < m_count; ++i)
	{
		if (m_bounds[i] == false)
		{
			m_points[i].PrepareMove(deltaTime, Force(i));

			if (m_prohibitedArea)
			{
				if (m_prohibitedArea->IsInArea(m_points[i].m_nextPos, m_points[i].m_radius, NULL))
				{
					UpdateOnCollision(i, deltaTime);
				}
			}
		}
	}

	AfterStep(deltaTime);

	for (unsigned int i = 0; i < m_count; ++i)
	{
		if (m_bounds[i] == false)
			m_points[i].UpdateMove();
	}
}

///////////////////////////////////////////////////////////////////////////////
void MaterialPointSetWithProhibitedArea::UpdateOnCollision(int id, double deltaTime)
{
	MaterialPoint *point = &m_points[id];
	Vec3d normal(0, 0, 0);
	if (m_prohibitedArea->IsInArea(point->m_nextPos, point->m_radius, &normal))
	{
		Vec3d force = Force(id);
		double normalFactor=DotProduct(force,normal);
		if(normalFactor<0){
			force-=normal * normalFactor;
		}

		double speedFactor = DotProduct(point->m_vel,normal);
		//if(speedFactor<0){
			point->m_vel-=(1+m_prohibitedArea->m_bounceFactor)*speedFactor* normal ;
		//}

		if(normalFactor<0 &&point->m_vel.Length()>0.0 ){
			Vec3d friction = -point->m_vel/10;
			friction.Normalize();
			friction *=fabs(m_prohibitedArea->m_frictionFactor*normalFactor);
			force+=friction;
		}
		point->PrepareMoveEuler(deltaTime, force);
	}
}